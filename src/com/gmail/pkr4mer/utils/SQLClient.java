package com.gmail.pkr4mer.utils;

import java.sql.*;

/*
 * NOTE: This Class and all other Classes, Interfaces and Packages inside the com.gmail.pkr4mer.utils Package ARE NOT exclusively a part of the plugin.
 * They were coded by me (Peter Kramer) for different plugins and/or testing purposes which are not connected to my work for Siege Isles.
 */

public class SQLClient {
	
	private String 	address;
	private int		port;
	private String		username;
	private String 	password;
	private String		database;
	private String 	url;
	private Connection	connection;
	private Statement	statement;
	
	public SQLClient( String addr, int prt, String usr, String pass, String db ) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		address	= addr;
		port	= prt;
		username= usr;
		password= pass;
		database= db;
		url = "jdbc:mysql://" + address + ":" + port + "/" + database;
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		connection = DriverManager.getConnection(url, username, password);
		statement = connection.createStatement();
	}
	
	public Connection getConnection()
	{
		return connection;
	}
	
	public void reconnect() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		connection.close();
		connection = DriverManager.getConnection(url, username, password);
		statement = connection.createStatement();
	}

	public int write( String querystring ) throws SQLException
	{
		//Bukkit.getLogger().info("SQLWRITE> " + querystring);
		statement = connection.createStatement();
		return statement.executeUpdate(querystring);
	}
	
	public ResultSet read( String querystring ) throws SQLException
	{
		//Bukkit.getLogger().info("SQLREAD> " + querystring);
		statement = connection.createStatement();
		return statement.executeQuery(querystring);
	}
}