package net.permslite;

import java.sql.SQLException;

import net.permslite.command.PermsCommandExecutor;
import net.permslite.database.DatabaseHandler;
import net.permslite.groups.GroupManager;
import net.permslite.groups.PlayerRegistry;
import net.permslite.http.HttpHandler;
import net.permslite.listeners.PlayerListener;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;


import com.gmail.pkr4mer.utils.SQLClient;

public class PermsLite extends JavaPlugin
{
	private static PermsLite instance = new PermsLite();
	private SQLClient client;
	private SyncThread thread;
	
	public static PermsLite getInstance()
	{
		return instance;
	}
	
	public void onEnable()
	{
		boolean load = HttpHandler.checkIps(Bukkit.getIp(), Bukkit.getPort());
		if(load)
		{
			instance = this;
			saveDefaultConfig();
			loadConfig();
			try {
				DatabaseHandler.createTables();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			getCommand("perms").setExecutor(new PermsCommandExecutor());
			Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
			loadGroups();
			thread = new SyncThread();
			thread.start();
		}
		else
		{
			this.getPluginLoader().disablePlugin(this);
		}
	}
	
	@SuppressWarnings("deprecation")
	public void onDisable()
	{
		GroupManager.getInstance().saveAll();
		PlayerRegistry.getInstance().saveAll();
		thread.stop();
	}
	
	public SQLClient getSQLClient()
	{
		return client;
	}
	
	private void loadConfig()
	{
		FileConfiguration fileConfig = this.getConfig();
		loadDataBase(fileConfig.getConfigurationSection("sql"));
	}
	
	private void loadDataBase(ConfigurationSection config)
	{
		String adress = config.getString("adress");
		int port = config.getInt("port");
		String username = config.getString("username");
		String pass = config.getString("password");
		String database = config.getString("database");
		try {
			client = new SQLClient(adress, port, username, pass, database);
		} catch (Exception e) {
			e.printStackTrace(); }
	}
	
	private void loadGroups()
	{
		for(String name : DatabaseHandler.getInstance().getGroupNames())
		{	
			GroupManager.getInstance().createGroup(name, DatabaseHandler.getInstance().getGroupPerms(name));
			GroupManager.getInstance().getGroup(name).setParents(DatabaseHandler.getInstance().getGroupParents(name));
			GroupManager.getInstance().getGroup(name).setPrefix(DatabaseHandler.getInstance().getGroupPrefix(name));
		}
	}
		
	public static long getUnixTime()
	{
		return System.currentTimeMillis();
	}
}
