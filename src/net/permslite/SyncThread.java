package net.permslite;

import net.permslite.groups.GroupManager;
import net.permslite.groups.PlayerRegistry;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


public class SyncThread extends Thread
{
	public SyncThread()
	{
		this.setName("SyncThreadPermsLite"); 	
	}
	
	@Override
	public void run()
	{
		while(true)
		{
			PlayerRegistry.getInstance().saveAll();
			GroupManager.getInstance().saveAll();
			for(Player player : Bukkit.getOnlinePlayers())
			{
				PlayerRegistry.getInstance().redoPermissions(PlayerRegistry.getInstance().getPlayer(player.getName()));
			}
			Bukkit.getLogger().info("Merged permissions with database.");
			try 
			{
				sleep(1000 * 60 * 10);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
	}
}
