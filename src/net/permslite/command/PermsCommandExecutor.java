package net.permslite.command;


import net.permslite.groups.Group;
import net.permslite.groups.GroupManager;
import net.permslite.groups.PermsPlayer;
import net.permslite.groups.PlayerRegistry;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;


public class PermsCommandExecutor implements CommandExecutor
{
	public static final String userUsage = "/perms user [name] [addperm|rmperm|setgroup|info] {permission|group}";
	public static final String groupUsage = "/perms group [name] [setprefix|addperm|rmperm|destroy|info|addparent|rmparent] {prefix|permission|parent}";
	public static final String createGroupUsage = "/perms creategroup [name] {prefix}";
	public static final String helpInfo = ChatColor.YELLOW + "Usage:\n"
											+ userUsage + "\n"
											+ groupUsage + "\n"
											+ createGroupUsage + "\n"
											+ "/perms listgroups";
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if( cmd.getName().equals("perms") )
		{
			if( args.length == 0 ) return false;
			if( args[0].equalsIgnoreCase("help") )
			{
				sender.sendMessage(helpInfo);
				return true;
			}
			else if( args[0].equalsIgnoreCase("reload") )
			{
				GroupManager.getInstance().saveAll();
				PlayerRegistry.getInstance().saveAll();
				sender.sendMessage(ChatColor.GREEN + "Reloaded.");
				return true;
			}
			else if( args[0].equalsIgnoreCase("user") )
			{
				if( args.length < 4 )
				{
					if( args.length == 3 && args[2].equalsIgnoreCase("info") )
					{
						PermsPlayer player = PermsPlayer.get(args[1]);
						String msg = ChatColor.GOLD + player.getName();
						msg += ChatColor.GOLD + "\nGroup: " + ChatColor.YELLOW + player.getGroup().getName();
						msg += ChatColor.GOLD + "\nPermissions:" + ChatColor.YELLOW;
						for( String perm : player.getPermissions() )
						{
							msg += " " + perm;
						}
						sender.sendMessage(msg);
						return true;
					}
					else
					{
						sender.sendMessage(userUsage);
						return true;
					}
				}
				PermsPlayer player = PermsPlayer.get(args[1]);
				if( args[2].equalsIgnoreCase("addperm") )
				{
					player.addPermission(args[3]);
					sender.sendMessage(ChatColor.GREEN + "Permission '" + ChatColor.YELLOW + args[3] + ChatColor.GREEN + "' added to player " + ChatColor.YELLOW + player.getName() + ChatColor.GREEN + ".");
				}
				else 
				if( args[2].equalsIgnoreCase("rmperm") )
				{
					player.removePermission(args[3]);
					sender.sendMessage(ChatColor.GOLD + "Permission '" + ChatColor.YELLOW + args[3] + ChatColor.GOLD + "' removed from player " + ChatColor.YELLOW + player.getName() + ChatColor.GOLD + ".");
				}
				else if( args[2].equalsIgnoreCase("setgroup") )
				{
					Group group = GroupManager.getInstance().getGroup(args[3]);
					if( group == null )
					{
						sender.sendMessage(ChatColor.RED + "This group does not exist.");
						return true;
					}
					player.setGroup(group);
					sender.sendMessage(ChatColor.GREEN + "Player " + ChatColor.YELLOW + player.getName() + ChatColor.GREEN + " has successfully been moved to the group " + ChatColor.YELLOW + group.getName() + ChatColor.GREEN + ".");
					if( player.getPlayer() != null )
					{
						player.getPlayer().sendMessage(ChatColor.YELLOW + "You were moved to the group " + group.getName() + ".");
					}
					return true;
				}
				else
				{
					sender.sendMessage(userUsage);
					return true;
				}
				return true;
			}
			else if( args[0].equalsIgnoreCase("group") )
			{
				if( args.length < 3 )
				{
					sender.sendMessage(groupUsage);
					return true;
				}
				Group group = GroupManager.getInstance().getGroup(args[1]);
				if( group == null )
				{
					sender.sendMessage(ChatColor.RED + "This group does not exist.");
					return true;
				}
				if( args[2].equalsIgnoreCase("addperm") )
				{
					if( args.length < 4 )
					{
						sender.sendMessage(groupUsage);
						return true;
					}
					group.addPermission(args[3]);
					sender.sendMessage(ChatColor.GREEN + "Permission '" + ChatColor.YELLOW + args[3] + ChatColor.GREEN + "' added to group " + ChatColor.YELLOW + group.getName() + ChatColor.GREEN + ".");
				}
				else if( args[2].equalsIgnoreCase("rmperm") )
				{
					if( args.length < 4 )
					{
						sender.sendMessage(groupUsage);
						return true;
					}
					group.removePermission(args[3]);
					sender.sendMessage(ChatColor.GOLD + "Permission '" + ChatColor.YELLOW + args[3] + ChatColor.GOLD + "' removed from group " + ChatColor.YELLOW + group.getName() + ChatColor.GOLD + ".");
				}
				else if(args[2].equalsIgnoreCase("setprefix") )
				{
					if( args.length < 4 )
					{
						sender.sendMessage(groupUsage);
						return true;
					}
					group.setPrefix(args[3]);
					sender.sendMessage(ChatColor.GREEN + "Prefix assigned.");
				}
				else if(args[2].equalsIgnoreCase("addparent") )
				{
					if( args.length < 4 )
					{
						sender.sendMessage(groupUsage);
						return true;
					}
					if( !GroupManager.getInstance().exists(args[3]) )
					{
						sender.sendMessage(ChatColor.RED + "Parent group not found.");
						return true;
					}
					group.addParent(args[3]);
					sender.sendMessage(ChatColor.GREEN + "Parent assigned.");
				}
				else if(args[2].equalsIgnoreCase("rmparent") )
				{
					if( args.length < 4 )
					{
						sender.sendMessage(groupUsage);
						return true;
					}
					if( !GroupManager.getInstance().exists(args[3]) )
					{
						sender.sendMessage(ChatColor.RED + "Parent group not found.");
						return true;
					}
					group.removeParent(args[3]);
					sender.sendMessage(ChatColor.GREEN + "Parent removed.");
				}
				else if(args[2].equalsIgnoreCase("destroy") )
				{
					if( group.destroy() )
					{
						sender.sendMessage(ChatColor.GREEN + "Group destroyed, members have been moved to the default group.");
						return true;
					}
					else
					{
						sender.sendMessage(ChatColor.RED + "Cannot destroy default group.");
						return true;
					}
				}
				else if(args[2].equalsIgnoreCase("info") )
				{
					String message = ChatColor.GOLD + group.getName();
					sender.sendMessage(message);
					message = ChatColor.GOLD + "Prefix: " + ChatColor.YELLOW + group.getPrefix() + ChatColor.GOLD;
					sender.sendMessage(message);
					message = ChatColor.GOLD + "Permissions:" + ChatColor.YELLOW;
					for( String node : group.getPermissions() )
					{
						message += " " + node;
					}
					sender.sendMessage(message);
					message = ChatColor.GOLD + "Parents:" + ChatColor.YELLOW;
					for( String parent : group.getParents() )
					{
						message += " " + parent;
					}
					sender.sendMessage(message);
					return true;
				}
				else
				{
					sender.sendMessage(groupUsage);
					return true;
				}
				return true;
			}
			else if( args[0].equalsIgnoreCase("creategroup") )
			{
				if( args.length < 2 )
				{
					sender.sendMessage(createGroupUsage);
					return true;
				}
				if( GroupManager.getInstance().exists(args[1]) )
				{
					sender.sendMessage(ChatColor.RED + "This group already exists.");
					return true;
				}
				if( GroupManager.getInstance().createGroup(args[1]) )
				{
					Group g = GroupManager.getInstance().getGroup(args[1]);
					if( args.length > 2 )
					{
						g.setPrefix(args[2]);
					}
					sender.sendMessage(ChatColor.GREEN + "Successfully created the group " + ChatColor.YELLOW + args[1] + ChatColor.GREEN + ".");
					return true;
				}
				else
				{
					sender.sendMessage(ChatColor.RED + "Failed to create the group.");
					return false;
				}
			}
			else if( args[0].equalsIgnoreCase("listgroups") )
			{
				String msg = ChatColor.YELLOW.toString();
				for( Group group : GroupManager.getInstance().getGroups() )
				{
					msg += group.getName() + " ";
				}
				sender.sendMessage(msg);
				return true;
			}
		}
		return false;
	}
}