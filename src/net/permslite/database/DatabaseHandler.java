package net.permslite.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import net.permslite.PermsLite;
import net.permslite.groups.Group;
import net.permslite.groups.GroupManager;
import net.permslite.groups.PermsPlayer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;


import com.gmail.pkr4mer.utils.SQLClient;

public class DatabaseHandler 
{
	private static String playerGroupTable = "player_groups";
	private static String playerPermsTable = "player_perms";
	private static String groupPermsTable = "group_perms";
	private static String groupParentsTable = "group_parents";
	private static String groupInfoTable = "group_info";
	
	private static DatabaseHandler instance = new DatabaseHandler();
	
	private DatabaseHandler() { }

	private HashMap<String,Long> playerPermsLastDownloaded = new HashMap<String,Long>();
	private HashMap<String,Long> playerGroupsLastDownloaded = new HashMap<String,Long>();
	private HashMap<String,Long> groupPermsLastDownloaded = new HashMap<String,Long>();
	private HashMap<String,Long> groupParentsLastDownloaded = new HashMap<String,Long>();
	
	public static DatabaseHandler getInstance()
	{
		return instance;
	}
	
	// works.
	public static void createTables() throws SQLException
	{
		SQLClient sqlClient = PermsLite.getInstance().getSQLClient();
		if( !sqlClient.getConnection().getMetaData().getTables(null, null, playerGroupTable, null).next() )
		{
			String query = 
					"CREATE TABLE " + playerGroupTable +
					"(" +
					"`name` VARCHAR(16) NOT NULL UNIQUE, " +
					"`group` VARCHAR(32)," +
					"`last_change` BIGINT" +
					")";
			sqlClient.write(query);
		}
		if( !sqlClient.getConnection().getMetaData().getTables(null, null, playerPermsTable, null).next() )
		{
			String query = 
					"CREATE TABLE " + playerPermsTable +
					"(" +
					"`name` VARCHAR(16) NOT NULL UNIQUE, " +
					"`permissions` TEXT" +
					")";
			sqlClient.write(query);
		}
		if( !sqlClient.getConnection().getMetaData().getTables(null, null, groupPermsTable, null).next() )
		{
			String query = 
					"CREATE TABLE " + groupPermsTable +
					"(" +
					"`group` VARCHAR(16) NOT NULL UNIQUE, " +
					"`permissions` TEXT" +
					")";
			sqlClient.write(query);
		}
		if( !sqlClient.getConnection().getMetaData().getTables(null, null, groupParentsTable, null).next() )
		{
			String query = 
					"CREATE TABLE " + groupParentsTable +
					"(" +
					"`group` VARCHAR(16) NOT NULL UNIQUE, " +
					"`parents` TEXT" +
					")";
			sqlClient.write(query);
		}
		if( !sqlClient.getConnection().getMetaData().getTables(null, null, groupInfoTable, null).next() )
		{
			String query = 
					"CREATE TABLE " + groupInfoTable +
					"(" +
					"`group` VARCHAR(16) NOT NULL UNIQUE, " +
					"`prefix` VARCHAR(32), " +
					"`last_change` BIGINT," +
					"`created` BIGINT" +
					")";
			sqlClient.write(query);
		}
	}
	
	// works.
	public HashMap<String,Long> getGroupPerms(String name)
	{
		this.groupPermsLastDownloaded.put(name, PermsLite.getUnixTime());
		SQLClient client = PermsLite.getInstance().getSQLClient();
		HashMap<String,Long> list = new HashMap<String,Long>();
		try
		{
			ResultSet result = client.read("SELECT `permissions` FROM " + groupPermsTable + " WHERE `group`='" + name + "'");
			if( result.next() )
			{
				if( !result.getString("permissions").equals(""))
					//FORMAT: "node time  node time  node time"
					for( String node : result.getString("permissions").split("  ") )
					{
						String perm = node.split(" ")[0];
						long time = Long.parseLong(node.split(" ")[1]);
						list.put(perm,time);
					}
			}
			else
			{
				return list;
			}
		}
		catch(Exception e){e.printStackTrace();}
		return list;
	}
	
	// works.
	public HashMap<String,Long> getGroupParents(String name)
	{
		this.groupParentsLastDownloaded.put(name, PermsLite.getUnixTime());
		SQLClient client = PermsLite.getInstance().getSQLClient();
		HashMap<String,Long> list = new HashMap<String,Long>();
		try
		{
			ResultSet result = client.read("SELECT `parents` FROM " + groupParentsTable + " WHERE `group`='" + name + "'");
			if( result.next() )
			{
				if( !result.getString("parents").equals(""))
					//FORMAT: "node time  node time  node time"
					for( String node : result.getString("parents").split("  ") )
					{
						String parent = node.split(" ")[0];
						long time = Long.parseLong(node.split(" ")[1]);
						list.put(parent,time);
					}
			}
			else
			{
				return list;
			}
		}
		catch(Exception e){e.printStackTrace();}
		return list;
	}
	
	//works.
	public ArrayList<String> getGroupNames()
	{
		ArrayList<String> list = new ArrayList<String>();
		SQLClient client = PermsLite.getInstance().getSQLClient();
		try{
			ResultSet result = client.read("SELECT `group` FROM " + groupInfoTable);
			while(result.next())
			{
				list.add(result.getString("group"));
			}
		} catch(Exception e){e.printStackTrace();}
		return list;
	}
	
	// works.
	public String getPlayerGroup(String name)
	{
		playerGroupsLastDownloaded.put(name, PermsLite.getUnixTime());
		SQLClient client = PermsLite.getInstance().getSQLClient();
		ResultSet result;
		try {
			result = client.read("SELECT `group` FROM "+playerGroupTable+" WHERE `name`='" + name + "'");
			if(result.next())
			{
				String group = result.getString("group");
				return group;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return GroupManager.getInstance().getDefault().getName();
	}
	
	// works.
	public long getPlayerGroupChange(String name)
	{
		playerGroupsLastDownloaded.put(name, PermsLite.getUnixTime());
		SQLClient client = PermsLite.getInstance().getSQLClient();
		ResultSet result;
		try {
			result = client.read("SELECT `last_change` FROM "+playerGroupTable+" WHERE `name`='" + name + "'");
			if(result.next())
			{
				if( result.getFetchSize() == 0 || result.getString(0) == null || result.getString(0).equals("") ) return 0;
				long change = result.getLong(0);
				return change;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	//works.
	public String getGroupPrefix(String name)
	{
		SQLClient client = PermsLite.getInstance().getSQLClient();
		ResultSet result;
		try {
			result = client.read("SELECT `prefix` FROM "+groupInfoTable+" WHERE `group`='" + name + "'");
			if( result.next() )
			{
				return result.getString("prefix");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "[" + name + "]";
	}
	
	//works.
	public long getGroupCreated(String name)
	{
		SQLClient client = PermsLite.getInstance().getSQLClient();
		ResultSet result;
		try {
			result = client.read("SELECT `created` FROM "+groupInfoTable+" WHERE `group`='" + name + "'");
			if( result.next() )
			{
				return result.getLong("created");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0L;
	}
	
	
	// Returns the permission nodes mapped to their unix timestamp
	public HashMap<String,Long> getPlayerPerms( String name)
	{
		HashMap<String,Long> list = new HashMap<String,Long>();
		SQLClient client = PermsLite.getInstance().getSQLClient();
		ResultSet result;
		try {
			result = client.read("SELECT `permissions` FROM " + playerPermsTable + " WHERE name='" + name + "'");
			if( result.next() )
			{
				if( result.getString("permissions").equals("") ) return list;
				//FORMAT: "node time  node time  node time"
				for( String node : result.getString("permissions").split("  ") )
				{
					list.put(node.split(" ")[0],Long.parseLong(node.split(" ")[1]));
				}
			}
			else
			{
				return list;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public boolean checkRemoteGroupExistence( String group )
	{
		SQLClient client = PermsLite.getInstance().getSQLClient();
		ResultSet result;
		try
		{
			result = client.read("SELECT `group` FROM "+ groupInfoTable + " WHERE `group`='" + group + "'");
			return result.next();
		}
		catch(Exception e)
		{
			return false;
		}
	}
	
	public void createPlayerInDatabase(String name)
	{
		String query1 = "INSERT INTO " + playerGroupTable + "(`name`,`group`) VALUES ('"+name+"','"+GroupManager.getInstance().getDefault().getName()+"')";
		String query2 = "INSERT INTO " + playerPermsTable + "(`name`,`permissions`) VALUES ('"+name+"','')";
		SQLClient sqlClient = PermsLite.getInstance().getSQLClient();
		try
		{
			sqlClient.write(query1);
		} catch(Exception e) {}
		try
		{
			sqlClient.write(query2);
		} catch(Exception e) {}
	}
	
	public void createGroupInDatabase(String name)
	{
		String query1 = "INSERT INTO " + groupInfoTable + "(`group`,`prefix`,`created`) VALUES ('"+name+"','"+GroupManager.getInstance().getGroup(name).getPrefix()+"','"+PermsLite.getUnixTime()+"')";
		String query2 = "INSERT INTO " + groupPermsTable + "(`group`,`permissions`) VALUES ('"+name+"','')";
		String query3 = "INSERT INTO " + groupParentsTable + "(`group`,`parents`) VALUES ('"+name+"','')";
		SQLClient sqlClient = PermsLite.getInstance().getSQLClient();
		try
		{
			sqlClient.write(query1);
		} catch(Exception e) {}
		try
		{
			sqlClient.write(query2);
		} catch(Exception e) {}
		try
		{
			sqlClient.write(query3);
		} catch(Exception e) {}
	}

	public long getPlayerLastPermissionDownload( String name )
	{
		if(!playerPermsLastDownloaded.containsKey(name)) return 0;
		return playerPermsLastDownloaded.get(name);
	}
	
	public long getGroupLastPermissionDownload( String name )
	{
		if(!groupPermsLastDownloaded.containsKey(name)) return 0;
		return groupPermsLastDownloaded.get(name);
	}
	
	public long getGroupLastParentsDownload( String name )
	{
		if(!groupParentsLastDownloaded.containsKey(name)) return 0;
		return groupParentsLastDownloaded.get(name);
	}
	
	// Forces a save to the database aswell as locally
	private void overridePlayerPerms( String name, HashMap<String,Long> perms )
	{
		String databaseString = "";
		for( String node : perms.keySet() )
		{
			databaseString += "  " + node + " " + perms.get(node);
		}
		if( databaseString.length() > 2 )
			databaseString = databaseString.substring(2);
		createPlayerInDatabase(name);
		String query = "UPDATE " + playerPermsTable + " SET `permissions`='" + databaseString + "' WHERE `name`='" + name + "'";
		SQLClient sqlClient = PermsLite.getInstance().getSQLClient();
		try
		{
			sqlClient.write(query);
		} catch(SQLException e) { e.printStackTrace(); }
		if( Bukkit.getPlayer(name) != null )
		{
			PermsPlayer.get(name).overridePermissions(perms);
		}
	}
	
	private void overridePlayerGroup( String name, String group, long time )
	{
		String query = "UPDATE " + playerGroupTable + " SET `group`='" + group + "',`last_change`='"+time+"' WHERE `name`='" + name + "'";
		SQLClient sqlClient = PermsLite.getInstance().getSQLClient();
		try
		{
			sqlClient.write(query);
		} catch(SQLException e) { e.printStackTrace(); }
		if( Bukkit.getPlayer(name) != null )
		{
			PermsPlayer.get(name).setGroup(GroupManager.getInstance().getGroup(group), time);
		}
	}
	
	public void moveToDefault(String group)
	{
		for( Player player : Bukkit.getOnlinePlayers() )
		{
			if( PermsPlayer.get(player.getName()).getGroup().getName().equalsIgnoreCase(group) )
			{
				player.sendMessage(ChatColor.YELLOW + "You were moved to the default group.");
				PermsPlayer.get(player.getName()).setGroup(GroupManager.getInstance().getDefault());
			}
		}
		String query = "UPDATE " + playerGroupTable + " SET `group`='" + GroupManager.getInstance().getDefault().getName() + "',`last_change`='"+PermsLite.getUnixTime()+"' WHERE `group`='" + group + "'";
		SQLClient sqlClient = PermsLite.getInstance().getSQLClient();
		try
		{
			sqlClient.write(query);
		} catch(SQLException e) { e.printStackTrace(); }
	}
	
	public void destroyRemotely( String group )
	{
		String query1 = "DELETE FROM " + groupInfoTable + " WHERE `group`='" + group + "'";
		String query2 = "DELETE FROM " + groupPermsTable + " WHERE `group`='" + group + "'";
		String query3 = "DELETE FROM " + groupParentsTable + " WHERE `group`='" + group + "'";
		SQLClient sqlClient = PermsLite.getInstance().getSQLClient();
		try
		{
			sqlClient.write(query1);
		} catch(SQLException e) { e.printStackTrace(); }
		try
		{
			sqlClient.write(query2);
		} catch(SQLException e) { e.printStackTrace(); }
		try
		{
			sqlClient.write(query3);
		} catch(SQLException e) { e.printStackTrace(); }
	}
	
	// Forces a save to the database aswell as locally
	private void overrideGroupPerms( String name, HashMap<String,Long> perms )
	{
		String databaseString = "";
		for( String node : perms.keySet() )
		{
			databaseString += "  " + node + " " + perms.get(node);
		}
		if( databaseString.length() > 2 )
			databaseString = databaseString.substring(2);
		createGroupInDatabase(name);
		String query = "UPDATE " + groupPermsTable + " SET `permissions`='" + databaseString + "' WHERE `group`='" + name + "'";
		SQLClient sqlClient = PermsLite.getInstance().getSQLClient();
		try
		{
			sqlClient.write(query);
		} catch(SQLException e) { e.printStackTrace(); }
		GroupManager.getInstance().getGroup(name).setPermissions(perms);
	}
	
	// Forces a save to the database aswell as locally
	private void overrideGroupParents( String name, HashMap<String,Long> parents )
	{
		String databaseString = "";
		for( String parent : parents.keySet() )
		{
			databaseString += "  " + parent + " " + parents.get(parent);
		}
		if( databaseString.length() > 2 )
			databaseString = databaseString.substring(2);
		createGroupInDatabase(name);
		String query = "UPDATE " + groupParentsTable + " SET `parents`='" + databaseString + "' WHERE `group`='" + name + "'";
		SQLClient sqlClient = PermsLite.getInstance().getSQLClient();
		try
		{
			sqlClient.write(query);
		} catch(SQLException e) { e.printStackTrace(); }
		GroupManager.getInstance().getGroup(name).setParents(parents);
	}

	private void overrideGroupPrefix(String group, String prefix, long time)
	{
		String query = "UPDATE " + groupInfoTable + " SET `prefix`='" + prefix + "',`last_change`='"+time+"' WHERE `group`='" + group + "'";
		createGroupInDatabase(group);
		SQLClient sqlClient = PermsLite.getInstance().getSQLClient();
		try
		{
			sqlClient.write(query);
		} catch(SQLException e) { e.printStackTrace(); }
		GroupManager.getInstance().getGroup(group).setPrefix(prefix);
	}
	
	// Merges a Map with permissions from the database, pushes the merge.
	public void mergePlayerPerms( String name, HashMap<String,Long> newPerms )
	{
		HashMap<String,Long> merged = new HashMap<String,Long>();
		HashMap<String,Long> databasePerms = getPlayerPerms(name);
		ArrayList<String> allNodes = new ArrayList<String>();
		for( String node : databasePerms.keySet() )
		{
			allNodes.add(node);
		}
		for( String node : newPerms.keySet() )
		{
			allNodes.add(node);
		}
		//UNIQUE
		allNodes = new ArrayList<String>(new HashSet<String>(allNodes));
		for( String node : allNodes )
		{
			if(newPerms.containsKey(node) && databasePerms.containsKey(node) )
			{
				if( newPerms.get(node) > databasePerms.get(node) )
				{
					merged.put(node,newPerms.get(node));
				}
				else
				{
					merged.put(node, databasePerms.get(node));
				}
			}
			else if(newPerms.containsKey(node) && !databasePerms.containsKey(node) )
			{
				// Node was added to player locally AFTER the last syncing.
				if( newPerms.get(node) > getPlayerLastPermissionDownload(name) )
				{
					merged.put(node,newPerms.get(node));
				}
			}
			else if(!newPerms.containsKey(node) && databasePerms.containsKey(node) )
			{
				// Node was added to player in db AFTER the last syncing
				if(databasePerms.get(node) > getPlayerLastPermissionDownload(name))
				{
					Bukkit.getLogger().info("Merging node '" + node + "', " + databasePerms.get(node) + " > " + getPlayerLastPermissionDownload(name));
					merged.put(node, databasePerms.get(node));
				}
			}
		}
		overridePlayerPerms(name,merged);
		if( Bukkit.getPlayer(name) != null )
		{
			PermsPlayer.get(name).redoPermissions();
		}
		playerPermsLastDownloaded.put(name, PermsLite.getUnixTime());
	}
	
	// Merges a Map with permissions from the database, pushes the merge.
	public void mergeGroupPerms( String group, HashMap<String,Long> newPerms )
	{
		if( !checkRemoteGroupExistence(group) )
		{
			Bukkit.getLogger().info(GroupManager.getInstance().getGroup(group).getCreated() + " > " + groupPermsLastDownloaded.get(group) + " | " + PermsLite.getUnixTime());
			if( GroupManager.getInstance().getGroup(group).getCreated() >= groupPermsLastDownloaded.get(group) )
			{
				HashMap<String,Long> merged = new HashMap<String,Long>();
				HashMap<String,Long> databasePerms = getGroupPerms(group);
				ArrayList<String> allNodes = new ArrayList<String>();
				for( String node : databasePerms.keySet() )
				{
					allNodes.add(node);
				}
				for( String node : newPerms.keySet() )
				{
					allNodes.add(node);
				}
				//UNIQUE
				allNodes = new ArrayList<String>(new HashSet<String>(allNodes));
				for( String node : allNodes )
				{
					if(newPerms.containsKey(node) && databasePerms.containsKey(node) )
					{
						if( newPerms.get(node) > databasePerms.get(node) )
						{
							merged.put(node,newPerms.get(node));
						}
						else
						{
							merged.put(node, databasePerms.get(node));
						}
					}
					else if(newPerms.containsKey(node) && !databasePerms.containsKey(node) )
					{
						if( newPerms.get(node) > getGroupLastPermissionDownload(group) )
						{
							merged.put(node,newPerms.get(node));
						}
					}
					else if(!newPerms.containsKey(node) && databasePerms.containsKey(node) )
					{
						if(databasePerms.get(node) > getGroupLastPermissionDownload(group))
						{
							merged.put(node, databasePerms.get(node));
						}
					}
				}
				overrideGroupPerms(group,merged);
				groupPermsLastDownloaded.put(group, PermsLite.getUnixTime());
			}
			else
			{
				GroupManager.getInstance().destroyGroup(group);
				groupPermsLastDownloaded.remove(group);
				groupParentsLastDownloaded.remove(group);
			}
		}
	}
	
	// Merges a Map with permissions from the database, pushes the merge.
	public void mergeGroupParents( String group, HashMap<String,Long> newParents)
	{
		if( !checkRemoteGroupExistence(group) )
		{
			if( GroupManager.getInstance().getGroup(group).getCreated() >= groupParentsLastDownloaded.get(group) )
			{
				HashMap<String,Long> merged = new HashMap<String,Long>();
				HashMap<String,Long> databaseParents = getGroupParents(group);
				ArrayList<String> allNodes = new ArrayList<String>();
				for( String node : databaseParents.keySet() )
				{
					allNodes.add(node);
				}
				for( String node : newParents.keySet() )
				{
					allNodes.add(node);
				}
				//UNIQUE
				allNodes = new ArrayList<String>(new HashSet<String>(allNodes));
				for( String node : allNodes )
				{
					if(newParents.containsKey(node) && databaseParents.containsKey(node) )
					{
						if( newParents.get(node) > databaseParents.get(node) )
						{
							merged.put(node,newParents.get(node));
						}
						else
						{
							merged.put(node, databaseParents.get(node));
						}
					}
					else if(newParents.containsKey(node) && !databaseParents.containsKey(node) )
					{
						if( newParents.get(node) > getGroupLastParentsDownload(group) )
						{
							Bukkit.getLogger().info(node + " | " + newParents.get(node) +" > "+ getGroupLastParentsDownload(group));
							merged.put(node,newParents.get(node));
						}
					}
					else if(!newParents.containsKey(node) && databaseParents.containsKey(node) )
					{
						if(databaseParents.get(node) > getGroupLastParentsDownload(group))
						{
							Bukkit.getLogger().info(node + " | " + databaseParents.get(node) +" > "+ getGroupLastParentsDownload(group));
							merged.put(node, databaseParents.get(node));
						}
					}
				}
				overrideGroupParents(group,merged);
				groupParentsLastDownloaded.put(group, PermsLite.getUnixTime());
			}
			else
			{
				GroupManager.getInstance().destroyGroup(group);
				groupPermsLastDownloaded.remove(group);
				groupParentsLastDownloaded.remove(group);
			}
		}
	}
	
	public void mergePlayerGroup(PermsPlayer player)
	{
		String databaseGroup = getPlayerGroup(player.getName());
		long databaseChange = getPlayerGroupChange(player.getName());
		String resultGroup;
		long resultChange;
		// Same group
		if( databaseGroup.equalsIgnoreCase(player.getGroup().getName()) )
		{
			if( databaseChange != player.getLastGroupChange() )
			{
				if( databaseChange > player.getLastGroupChange() )
				{
					resultChange = databaseChange;
				}
				else
				{
					resultChange = player.getLastGroupChange();
				}
				resultGroup = databaseGroup;
			}
			else
			{
				resultChange = databaseChange;
				resultGroup = databaseGroup;
			}
		}
		else
		{
			if( databaseChange > player.getLastGroupChange() )
			{
				resultGroup = databaseGroup;
				resultChange = databaseChange;
			}
			else
			{
				resultGroup = player.getGroup().getName();
				resultChange = player.getLastGroupChange();
			}
		}
		Bukkit.getLogger().info("Merged Player group: " + player.getName()+ "->" + resultGroup);
		overridePlayerGroup(player.getName(),resultGroup,resultChange);
	}
	
	public void mergeGroupPrefix(Group group)
	{
		if(!checkRemoteGroupExistence(group.getName()))
		{
			if( group.getCreated() >= groupPermsLastDownloaded.get(group.getName()) )
			{
				String databasePrefix = getGroupPrefix(group.getName());
				long databaseChange = group.getLastPrefixChange();
				String resultPrefix;
				long resultChange;
				// Same group
				if( databasePrefix.equalsIgnoreCase(group.getPrefix()) )
				{
					if( databaseChange != group.getLastPrefixChange() )
					{
						if( databaseChange > group.getLastPrefixChange() )
						{
							resultChange = databaseChange;
						}
						else
						{
							resultChange = group.getLastPrefixChange();
						}
						resultPrefix = databasePrefix;
					}
					else
					{
						resultChange = databaseChange;
						resultPrefix = databasePrefix;
					}
				}
				else
				{
					if( databaseChange > group.getLastPrefixChange() )
					{
						resultPrefix = databasePrefix;
						resultChange = databaseChange;
					}
					else
					{
						resultPrefix = group.getPrefix();
						resultChange = group.getLastPrefixChange();
					}
				}
				overrideGroupPrefix(group.getName(),resultPrefix,resultChange);
			}
			else
			{
				GroupManager.getInstance().destroyGroup(group.getName());
				groupPermsLastDownloaded.remove(group.getName());
				groupParentsLastDownloaded.remove(group.getName());
			}
		}
	}

	public void registerPermissionDownload(PermsPlayer player)
	{
		playerPermsLastDownloaded.put(player.getName(), PermsLite.getUnixTime());
	}
	
	public void registerPermissionDownload(Group group)
	{
		groupPermsLastDownloaded.put(group.getName(), PermsLite.getUnixTime());
	}
	
	public void registerParentsDownload(Group group)
	{
		groupPermsLastDownloaded.put(group.getName(), PermsLite.getUnixTime());
	}
}