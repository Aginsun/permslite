package net.permslite.groups;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import net.permslite.PermsLite;
import net.permslite.database.DatabaseHandler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;


public class Group 
{
	private String name;
	private String prefix;
	private long lastPrefixChange;
	private long created;
	private HashMap<String,Long> permissions = new HashMap<String,Long>();
	private HashMap<String,Long> parents = new HashMap<String,Long>();
	
	public Group(String name)
	{
		this.name = name;
		this.prefix = "[" + name + "]";
		this.lastPrefixChange = 0L;
		this.created = PermsLite.getUnixTime();
		permissions = new HashMap<String,Long>();
		DatabaseHandler.getInstance().registerPermissionDownload(this);
		DatabaseHandler.getInstance().registerParentsDownload(this);
	}
	
	public Group(String name, HashMap<String,Long> map)
	{
		this.name = name;
		this.prefix = "[" + name + "]";
		this.lastPrefixChange = 0L;
		this.created = PermsLite.getUnixTime();
		permissions = map;
		DatabaseHandler.getInstance().registerPermissionDownload(this);
		DatabaseHandler.getInstance().registerParentsDownload(this);
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getPrefix()
	{
		return prefix;
	}
	
	public String getColoredPrefix()
	{
		return ChatColor.translateAlternateColorCodes('&', prefix);
	}
	
	public void setPrefix( String prefix )
	{
		this.prefix = prefix;
		this.lastPrefixChange = PermsLite.getUnixTime();
	}
	
	public void setPrefix( String prefix, long time )
	{
		this.prefix = prefix;
		this.lastPrefixChange = time;
	}
	
	public void setCreated( long time )
	{
		this.created = time;
	}
	
	public void removeParent(String group)
	{
		parents.remove(group);
	}
	
	public void addParent(String group)
	{
		parents.put(group, PermsLite.getUnixTime());
		for( Group g : GroupManager.getInstance().getGroups() )
		{
			g.updateMembers();
		}
		updateMembers();
	}
	
	public void updateMembers()
	{
		for( Player player : Bukkit.getOnlinePlayers() )
		{
			if(PermsPlayer.get(player.getName()).getGroup() == this)
				PermsPlayer.get(player.getName()).redoPermissions();
		}
	}
	
	public void addParent(String group, long time)
	{
		parents.put(group, time);
	}
	
	public boolean hasParent(String group)
	{
		return parents.containsKey(group);
	}
	
	public long getLastPrefixChange()
	{
		return lastPrefixChange;
	}
	
	public long getCreated()
	{
		return created;
	}
	
	public ArrayList<String> getPermissions()
	{
		ArrayList<String> perms = new ArrayList<String>();
		for( String p : permissions.keySet() )
		{
			perms.add(p);
		}
		for( String parent : parents.keySet().toArray(new String[parents.size()]) )
		{
			Group p = GroupManager.getInstance().getGroup(parent);
			if(p == null)
			{
				removeParent(parent);
				continue;
			}
			if( p.hasParent(this.getName()) )
			{
				PermsLite.getInstance().getLogger().info("Inheritance loop detected!!! ("+getName()+" <-> "+p.getName()+")");
				continue;
			}
			for( String perm : p.getPermissions() )
			{
				perms.add(perm);
			}
		}
		return perms;
	}
	
	public Set<String> getParents()
	{
		return parents.keySet();
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String,Long> getDetailedPermissions()
	{
		return (HashMap<String,Long>) permissions.clone();
	}
	
	public void addPermission(String permission)
	{
		permissions.put(permission,PermsLite.getUnixTime());
		updateMembers();
	}
	
	public boolean destroy()
	{
		return GroupManager.getInstance().destroyGroup(name);
	}
	
	public void setPermissions(HashMap<String,Long> perms)
	{
		permissions = perms;
		updateMembers();
	}
	
	public void setParents(HashMap<String,Long> parents)
	{
		this.parents = parents;
		updateMembers();
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String,Long> getDetailedParents()
	{
		return (HashMap<String,Long>)parents.clone();
	}
	
	public void removePermission(String permission)
	{
		permissions.remove(permission);
		updateMembers();
	}
	
	public boolean hasPermission(String perm)
	{
		for( String p : getPermissions() )
		{
			if( p.equalsIgnoreCase(perm) ) return true;
		}
		return false;
	}
}
