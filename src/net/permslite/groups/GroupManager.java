package net.permslite.groups;

import java.util.ArrayList;
import java.util.HashMap;

import net.permslite.database.DatabaseHandler;

import org.bukkit.permissions.Permission;


public class GroupManager
{
	private static GroupManager instance = new GroupManager();
	private HashMap<String,Group> groups;
	private String defaultGroup;
	
	private GroupManager()
	{
		groups = new HashMap<String,Group>();
		defaultGroup = "default";
	}
	
	public static GroupManager getInstance()
	{
		return instance;
	}
	
	public boolean setDefault(String group)
	{
		if(!exists(group)) return false;
		defaultGroup = group;
		return true;
	}
	
	public Group getDefault()
	{
		if(createGroup("default"))
		{
			getDefault().setPrefix("&4{Default}");
		}
		return getGroup(defaultGroup);
	}
	
	public boolean exists(String group)
	{
		boolean e = false;
		for( String g : groups.keySet() )
		{
			if( g.equalsIgnoreCase(group) ) e = true;
		}
		return e;
	}
	
	public boolean createGroup( String group )
	{
		if( exists(group) ) return false;
		groups.put(group, new Group(group));
		return true;
	}
	
	public boolean createGroup ( String group, HashMap<String,Long> perms )
	{
		if( exists(group) ) return false;
		groups.put(group, new Group(group, perms));
		return true;
	}
	
	public boolean isDefault( Group group )
	{
		return group.getName().equalsIgnoreCase(defaultGroup);
	}
	
	public boolean destroyGroup( String group )
	{
		if(!exists(group)) return false;
		if(isDefault(getGroup(group))) return false;
		DatabaseHandler.getInstance().moveToDefault(group);
		DatabaseHandler.getInstance().destroyRemotely(group);
		for( Group g : groups.values() )
		{
			if( g.hasParent(group) ) g.removeParent(group);
		}
		for( String g : groups.keySet() )
		{
			if( g.equalsIgnoreCase(group) )
			{
				groups.remove(g);
				return true;
			}
		}
		return false;
	}
	
	public Group getGroup( String group )
	{
		for( String g : groups.keySet() )
		{
			if( g.equalsIgnoreCase(group) ) return groups.get(g);
		}
		return null;
	}
	
	public ArrayList<Group> getGroups()
	{
		ArrayList<Group> gs = new ArrayList<Group>();
		for( Group g : groups.values() )
		{
			gs.add(g);
		}
		return gs;
	}
	
	public ArrayList<Group> getGroupsWithAccess(Permission perm)
	{
		ArrayList<Group> results = new ArrayList<Group>();
		for( Group group : groups.values() )
		{
			if( group.hasPermission(perm.getName()) )
			{
				results.add(group);
			}
		}
		return results;
	}
	
	public void saveAll()
	{
		for( String group : groups.keySet().toArray(new String[groups.size()]) )
		{
			
			try
			{
				DatabaseHandler.getInstance().mergeGroupPerms(group,groups.get(group).getDetailedPermissions());
				DatabaseHandler.getInstance().mergeGroupParents(group,groups.get(group).getDetailedParents());
				DatabaseHandler.getInstance().mergeGroupPrefix(groups.get(group));
			}catch( Exception e ) {}
		}
		downloadNewGroups();
	}
	
	private void downloadNewGroups()
	{
		for( String remoteGroup : DatabaseHandler.getInstance().getGroupNames() )
		{
			if(!exists(remoteGroup))
			{
				createGroup(remoteGroup, DatabaseHandler.getInstance().getGroupPerms(remoteGroup));
				getGroup(remoteGroup).setParents(DatabaseHandler.getInstance().getGroupParents(remoteGroup));
				getGroup(remoteGroup).setPrefix(DatabaseHandler.getInstance().getGroupPrefix(remoteGroup));
			}
		}
	}
}