package net.permslite.groups;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PermsPlayer
{
	private String name;
	
	public static PermsPlayer get(String name)
	{
		return PlayerRegistry.getInstance().getPlayer(name);
	}
	
	public PermsPlayer(String name)
	{
		this.name = name;
	}
	
	public Player getPlayer()
	{
		return Bukkit.getPlayerExact(name);
	}
	
	public Group getGroup()
	{
		return PlayerRegistry.getInstance().getGroup(this);
	}
	
	public void setGroup( Group group )
	{
		PlayerRegistry.getInstance().setGroup(this, group);
	}
	
	public void setGroup( Group group, long time )
	{
		PlayerRegistry.getInstance().setGroup(this, group, time);
	}
	
	public long getLastGroupChange()
	{
		return PlayerRegistry.getInstance().getLastGroupChange(this);
	}
	
	public void addPermission( String permission )
	{
		PlayerRegistry.getInstance().addPermission(this, permission);
	}
	
	public void removePermission( String permission )
	{
		PlayerRegistry.getInstance().removePermission(this, permission);
	}
	
	public ArrayList<String> getPermissions()
	{
		return PlayerRegistry.getInstance().getPlayerPerms(this);
	}
	
	public String getName()
	{
		return name;
	}
	
	public void redoPermissions()
	{
		PlayerRegistry.getInstance().redoPermissions(this);
	}
	
	public void overridePermissions( HashMap<String, Long> permissions )
	{
		PlayerRegistry.getInstance().setPlayerPerms(this, permissions);
	}
}