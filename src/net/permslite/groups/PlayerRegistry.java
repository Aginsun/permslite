package net.permslite.groups;

import java.util.ArrayList;
import java.util.HashMap;

import net.permslite.PermsLite;
import net.permslite.database.DatabaseHandler;

import org.bukkit.Bukkit;
import org.bukkit.permissions.PermissionAttachment;


public class PlayerRegistry 
{
	private static PlayerRegistry instance = new PlayerRegistry();
	private HashMap<String, PermsPlayer> players = new HashMap<String, PermsPlayer>();
	private HashMap<PermsPlayer, PermissionAttachment> attachments = new HashMap<PermsPlayer, PermissionAttachment>();
	private HashMap<PermsPlayer, HashMap<String,Long>> specificPerms = new HashMap<PermsPlayer, HashMap<String,Long>>();
	private HashMap<PermsPlayer, Group> playerGroups = new HashMap<PermsPlayer,Group>();
	private HashMap<PermsPlayer, Long> lastPlayerGroupChange = new HashMap<PermsPlayer,Long>();
	
	private PlayerRegistry() { }
	
	public static PlayerRegistry getInstance()
	{
		return instance;
	}
	
	public ArrayList<String> getPlayerPerms(PermsPlayer player)
	{
		ArrayList<String> hasPerms = new ArrayList<String>();
		for( String perm : specificPerms.get(player).keySet() )
		{
			hasPerms.add(perm);
		}
		return hasPerms;
	}
	
	protected void setPlayerPerms(PermsPlayer player, HashMap<String,Long> perms)
	{
		specificPerms.put(player, perms);
		redoPermissions(player);
	}

	protected void addPermission( PermsPlayer player, String perm )
	{
		specificPerms.get(player).put(perm,PermsLite.getUnixTime());
		player.redoPermissions();
	}
	
	public void redoPermissions(PermsPlayer player)
	{
		if( player.getPlayer() == null || !PermsLite.getInstance().isEnabled() ) return;
		attachments.get(player).remove();
		attachments.put(player, player.getPlayer().addAttachment(PermsLite.getInstance()));
		for( String perm : getGroup(player).getPermissions() )
		{
			attachments.get(player).setPermission(perm, true);
		}
		for( String perm : specificPerms.get(player).keySet() )
		{
			attachments.get(player).setPermission(perm, true);
		}
		player.getPlayer().recalculatePermissions();
	}
	
	protected void removePermission( PermsPlayer player, String perm )
	{
		ArrayList<String> remove = new ArrayList<String>();
		for( String p : specificPerms.get(player).keySet() )
		{
			if( p.equalsIgnoreCase(perm) )
			{
				remove.add(p);
			}
		}
		for( String p : remove )
		{
			specificPerms.get(player).remove(p);
			if(attachments.containsKey(player)) attachments.get(player).setPermission(p, false);
		}
		redoPermissions(player);
	}
	
	protected boolean hasPermission( PermsPlayer player, String perm )
	{
		if( attachments.containsKey(player) )
		{
			return attachments.get(player).getPermissions().get(perm);
		}
		else
		{
			return ( player.getGroup().hasPermission(perm) || player.getPermissions().contains(perm) );
		}
	}
	
	public PermsPlayer getPlayer(String name)
	{
		for( String player : players.keySet() )
		{
			if( player.equalsIgnoreCase(name) )
			{
				return players.get(player);
			}
		}
		addPlayer(name);
		return getPlayer(name);
	}
	
	public PermissionAttachment getAttachment(PermsPlayer player)
	{
		return attachments.get(player);
	}
	
	public void addPlayer(String name)
	{
		DatabaseHandler.getInstance().createPlayerInDatabase(name);
		PermsPlayer player = new PermsPlayer(name);
		players.put(name, player);
		specificPerms.put(player, DatabaseHandler.getInstance().getPlayerPerms(name));
		if(PermsLite.getInstance().isEnabled() && player.getPlayer() != null) attachments.put(player, player.getPlayer().addAttachment(PermsLite.getInstance()));
		playerGroups.put(player, GroupManager.getInstance().getGroup(DatabaseHandler.getInstance().getPlayerGroup(name)));
		lastPlayerGroupChange.put(player, DatabaseHandler.getInstance().getPlayerGroupChange(name));
		redoPermissions(player);
		DatabaseHandler.getInstance().registerPermissionDownload(player);
	}
	
	public void removePlayer(String name)
	{
		PermsPlayer player = PermsPlayer.get(name);
		playerGroups.remove(player);
		specificPerms.remove(player);
		players.remove(name);
		lastPlayerGroupChange.remove(player);
		Bukkit.getPlayer(name).removeAttachment(attachments.get(player));
		attachments.remove(player);
	}
	
	protected Group getGroup(PermsPlayer player)
	{
		Group group = playerGroups.get(player);
		if(GroupManager.getInstance().getGroup(group.getName())==null)
		{
			player.setGroup(GroupManager.getInstance().getDefault());
		}
		return playerGroups.get(player);
	}
	
	protected void setGroup( PermsPlayer player, Group group )
	{
		playerGroups.put(player, group);
		lastPlayerGroupChange.put(player, PermsLite.getUnixTime());
	}
	
	protected void setGroup( PermsPlayer player, Group group, long time )
	{
		playerGroups.put(player, group);
		lastPlayerGroupChange.put(player, time);
	}
	
	public long getLastGroupChange( PermsPlayer player )
	{
		return lastPlayerGroupChange.get(player);
	}
	
	public void saveAll()
	{
		for( PermsPlayer player : players.values() )
		{
			DatabaseHandler.getInstance().mergePlayerPerms(player.getName(), specificPerms.get(player));
			DatabaseHandler.getInstance().mergePlayerGroup(player);
		}
	}
}