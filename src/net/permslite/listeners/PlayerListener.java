package net.permslite.listeners;

import net.permslite.groups.PermsPlayer;
import net.permslite.groups.PlayerRegistry;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;


public class PlayerListener implements Listener
{
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		PlayerRegistry.getInstance().addPlayer(event.getPlayer().getName());
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event)
	{
		PlayerRegistry.getInstance().removePlayer(event.getPlayer().getName());
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void onPlayerChat(AsyncPlayerChatEvent evt)
	{
		evt.setFormat(PermsPlayer.get(evt.getPlayer().getName()).getGroup().getColoredPrefix() + ChatColor.RESET + " <%s> %s");
	}
}
